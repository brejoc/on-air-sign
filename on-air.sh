#!/usr/bin/env bash

video_dev="/dev/video*"
hub="1-1.2"
port="1"

prev_cam_status=0

function light_off() {
	uhubctl -l $hub -a 0 -p $port
}

function light_on() {
	uhubctl -l $hub -a 1 -p $port
}

light_off

while :
do
	fuser $video_dev &>/dev/null
	status=$?

	if [[ $status -eq 0 ]]
	then
		if [[ $prev_cam_status -eq 0 ]]
		then
			light_on
			prev_cam_status=1
		fi
	else
		if [[ $prev_cam_status -eq 1 ]]
		then
			light_off
			prev_cam_status=0
		fi
	fi
	sleep 1
done
